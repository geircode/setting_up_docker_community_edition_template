# Setting_up_Docker_Community_Edition


Usage:

- Run *Dockerfile.build.bat* first if you want to create your own Container Image.
- Run *docker-compose.up.bat* to start the workspace container

Portainer:

- Run *portainer\docker-compose.up.bat* to start Portainer on the local Docker Swarm
