REM Run Dockerfile.build.bat first if you want to create your own Container Image.
docker-compose -f docker-compose.yml down --remove-orphans
docker-compose -f docker-compose.yml up -d --build --remove-orphans
pause
docker exec -it docker_ce_19f93204-1 /bin/bash