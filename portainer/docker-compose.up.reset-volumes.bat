﻿
REM *** Requirements ***
REM Docker For Windows: Remember to tick the box in Docker > Settings > General:“Expose daemon on tcp://localhost:2375 without TLS”.
REM Run "docker swarm init" before running this bat-file.

cd %~dp0

docker stack rm portainer
docker volume rm portainer_portainer_data
docker volume rm portainer_5bd7f1c8_portainer_data
docker stack deploy -c docker-compose.yml portainer
pause
