# Commands used in this setup

## Most important commands

```shell
docker run -ti docker4x/create-sp-azure docker_ce_19f93204 docker-ce-19f93204_rg westeurope
ssh -i /ubuntu/geircode_19f93204_rsa -p 50000 docker@<Docker Swarm IP>
```

## SSH KEY Certificate

First, create certificate outside the volume share:

```shell
mkdir /ubuntu
ssh-keygen -t rsa -b 4096 -C geircode@geircode.no -f /ubuntu/geircode_19f93204_rsa
```

Copy the certificate files to local volume share for persistance:

```shell
cp -r /ubuntu /app/ubuntu
chmod 0400 /ubuntu/geircode_19f93204_rsa.pub
chmod 0400 /ubuntu/geircode_19f93204_rsa
```

## Creating Azure resources

```shell
docker pull docker4x/create-sp-azure:latest
# docker run -ti docker4x/create-sp-azure sp-name [rg-name rg-region]
docker run -ti docker4x/create-sp-azure docker_ce_19f93204 docker-ce-19f93204_rg westeurope
```

Login to Docker Swarm manager. Get the IP and Port from "externalSSHLoadBalancer - Inbound NAT rules":

```shell
# Example
ssh -i /ubuntu/geircode_19f93204_rsa -p 50000 docker@13.80.106.142
```

## Docker Swarm init with ssh-agent

```shell
swarm-manager000000:~$ docker swarm init
Swarm initialized: current node (py4y0q2jmk8jgi806nc0jofgc) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-591r0uu76vnar02h8f16n1e2p5p0dbt2pzpm4g89o0zrymhn39-a9q5n6bv4svr3amjscmvp2x55 10.0.0.5:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

swarm-manager000000:~$ docker node ls
ID                            HOSTNAME              STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
py4y0q2jmk8jgi806nc0jofgc *   swarm-manager000000   Ready               Active              Leader              18.03.0-ce
```

```text
swarm-manager000000:~$ ssh-add
Could not open a connection to your authentication agent.

root@40198a525e8f:/ubuntu# eval `ssh-agent -s`
Agent pid 96

root@40198a525e8f:/ubuntu# ssh-add -L
The agent has no identities.
root@40198a525e8f:/ubuntu# ssh-add /ubuntu/geircode_19f93204_rsa
Enter passphrase for /ubuntu/geircode_19f93204_rsa:
Identity added: /ubuntu/geircode_19f93204_rsa (/ubuntu/geircode_19f93204_rsa)
root@40198a525e8f:/ubuntu#

root@40198a525e8f:/ubuntu# ssh -p 50000 -A docker@40.74.57.141
Welcome to Docker!
swarm-manager000000:~$

swarm-manager000000:~$ ssh -A docker@10.0.0.4
The authenticity of host '10.0.0.4 (10.0.0.4)' can't be established.
RSA key fingerprint is SHA256:c260W6he0ppCfmik+oa7TN42K4/xfPigAK2VysCSe6U.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '10.0.0.4' (RSA) to the list of known hosts.
Welcome to Docker!
swarm-worker000000:~$

swarm-worker000000:~$ docker swarm join --token SWMTKN-1-591r0uu76vnar02h8f16n1e2p5p0dbt2pzpm4g89o0zrymhn39-a9q5n6bv4svr3amjscmvp2x55 10.0.0.5:2377
This node joined a swarm as a worker.
swarm-worker000000:~$
```

## Test setup with an non-trivial app

Login Swarm Manager node and run:

```shell
git clone https://github.com/dockersamples/example-voting-app.git
cd example-voting-app/
docker stack deploy --compose-file docker-stack.yml vote
```

List the tasks in the stack
swarm-manager000000:~/example-voting-app$ docker stack ps vote
ID                  NAME                IMAGE                                          NODE                  DESIRED STATE       CURRENT STATE              ERROR               PORTS
hi4sza5sxh25        vote_result.1       dockersamples/examplevotingapp_result:before   swarm-worker000000    Running             Preparing 27 seconds ago
fl4yd44htgdo        vote_vote.1         dockersamples/examplevotingapp_vote:before     swarm-worker000000    Running             Running 16 seconds ago
7ops9hpc687c        vote_db.1           postgres:9.4                                   swarm-manager000000   Running             Preparing 44 seconds ago
zribncm2ycfq        vote_redis.1        redis:alpine                                   swarm-worker000000    Running             Running 40 seconds ago
j2v5z2dkrtzn        vote_visualizer.1   dockersamples/visualizer:stable                swarm-manager000000   Running             Preparing 53 seconds ago
9o6sp5i1iv4y        vote_worker.1       dockersamples/examplevotingapp_worker:latest   swarm-manager000000   Running             Preparing 55 seconds ago
oixtfjsx8q33        vote_vote.2         dockersamples/examplevotingapp_vote:before     swarm-manager000000   Running             Preparing 34 seconds ago
swarm-manager000000:~/example-voting-app$

swarm-manager000000:~/example-voting-app$ docker node ls
ID                            HOSTNAME              STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
py4y0q2jmk8jgi806nc0jofgc *   swarm-manager000000   Ready               Active              Leader              18.03.0-ce
1qv3m227fz9n9mz3fg0trwhxt     swarm-worker000000    Ready               Active                                  18.03.0-ce
6453xo5zpbeafn36j2cevlwzh     swarm-worker000001    Ready               Active                                  18.03.0-ce
swarm-manager000000:~/example-voting-app$

swarm-manager000000:~/example-voting-app$ docker stack deploy --compose-file docker-stack.yml vote
Creating network vote_backend
Creating network vote_frontend
Creating network vote_default
Creating service vote_result
Creating service vote_worker
Creating service vote_visualizer
Creating service vote_redis
Creating service vote_db
Creating service vote_vote
swarm-manager000000:~/example-voting-app$

docker service create --constraint node.role==worker --name nginx --publish published=5015,target=80 nginx

docker stack deploy --compose-file docker-stack.yml vote